/* Copyright (C) 2012 - Hyogi Sim <hyogi@cs.vt.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>
#include <pthread.h>
#include <errno.h>
#include <linux/types.h>
#include <unistd.h>

#include <SDL.h>

extern int errno;

struct kpoint;
struct kcluster;

#define	MAX_DIMENSION		3
#define MAX_RAND		100
#define	SCREEN_WIDTH		800
#define	SCREEN_HEIGHT		600

struct kpoint {
	struct kcluster *affinity;
	double distance;
	double coord[MAX_DIMENSION];
};

struct kcluster {
	struct kpoint *center;
	int npoints;
	pthread_mutex_t lock;
	__u32 color;
};

struct work_data {
	struct kcluster *cluster;
	struct kpoint *points;
	int npoints;
};

static int dimension;
static int nthreads;
static int nclusters;
static int npoints;
static int moves;
static double *sumbuf;
static struct kcluster *clusters;
static struct kpoint *points;
static pthread_mutex_t global_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_barrier_t barrier;

static double get_distance(struct kpoint *p1, struct kpoint *p2)
{
	int i;
	double tmp, sum = 0.0f;

	for (i = 0; i < dimension; i++) {
		tmp = p1->coord[i] - p2->coord[i];
		sum += tmp * tmp;
	}

	return sqrt(sum);
}

static void dump_point(struct kpoint *point)
{
	int i;

	printf("(");
	for (i = 0; i < dimension; i++)
		printf("%lf%s", point->coord[i],
				i == dimension - 1 ? ")" : ", ");
}

static void dump_clusters(void)
{
	int i;

	for (i = 0; i < nclusters; i++) {
		struct kcluster *cluster = &clusters[i];
		printf("===[cluster%d]===\n", i);
		printf("center : ");
		dump_point(cluster->center);
		printf("\nnpoints: %d\n", cluster->npoints);
	}
}

static inline void dec_npoints(struct kcluster *cluster)
{
	if (!cluster)
		return;

	pthread_mutex_lock(&cluster->lock);
	if (cluster->npoints > 0)
		cluster->npoints--;
	pthread_mutex_unlock(&cluster->lock);
}

static inline void inc_npoints(struct kcluster *cluster)
{
	if (!cluster)
		return;

	pthread_mutex_lock(&cluster->lock);
	cluster->npoints++;
	pthread_mutex_unlock(&cluster->lock);
}

static inline void inc_moves(void)
{
	pthread_mutex_lock(&global_lock);
	moves++;
	pthread_mutex_unlock(&global_lock);
}

static struct kcluster *find_my_cluster(struct kpoint *point,
					struct kcluster *clusters)
{
	int i;
	double distance;
	double min = point->distance ? point->distance : DBL_MAX;
	struct kcluster *current, *mine, *old;

	old = mine = point->affinity;

	for (i = 0; i < nclusters; i++) {
		current = &clusters[i];
		distance = get_distance(point, current->center);
		if (distance < min) {
			min = distance;
			mine = current;
		}
	}

	if (mine != point->affinity) {
		dec_npoints(old);
		inc_npoints(mine);
		inc_moves();
		point->affinity = mine;
		point->distance = min;
	}

	return mine;
}

static void adjust_center(struct kcluster *cluster)
{
	int i, d, count = 0;
	double *sum = sumbuf;
	struct kpoint *current;
	struct kpoint *center = cluster->center;

	if (!sum)
		sum = malloc(sizeof(double) * dimension);

	memset(sum, 0, sizeof(double) * dimension);

	for (i = 0; i < npoints; i++) {
		current = &points[i];
		if (current->affinity != cluster)
			continue;

		for (d = 0; d < dimension; d++)
			sum[d] += current->coord[d];
		count++;
	}

	for (d = 0; d < dimension; d++)
		center->coord[d] = sum[d] / count;

	cluster->npoints = count;
}

static void *kmeans_work(void *arg)
{
	struct work_data *data = (struct work_data *) arg;
	struct kpoint *my_points = data->points;
	int i, my_npoints = data->npoints;

	do {
		for (i = 0; i < my_npoints; i++)
			(void) find_my_cluster(&my_points[i], clusters);

		pthread_barrier_wait(&barrier);

		/* wait for the main thread to adjust center */

		pthread_barrier_wait(&barrier);
	} while (moves > 0);

	pthread_exit(0);
}

/* This function is borrowed from some website, which I don't remember. */
static void putpixel(SDL_Surface *surface, int x, int y, __u32 pixel)
{
	int bpp = surface->format->BytesPerPixel;
	__u8 *p = (__u8 *)surface->pixels + y * surface->pitch + x * bpp;

	switch(bpp)
	{
	case 1:
		*p = pixel;
		break;
	case 2:
		*(__u16 *)p = pixel;
		break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
			p[0] = (pixel >> 16) & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = pixel & 0xff;
		}
		else {
			p[0] = pixel & 0xff;
			p[1] = (pixel >> 8) & 0xff;
			p[2] = (pixel >> 16) & 0xff;
		}
		break;
	case 4:
		*(__u32 *)p = pixel;
		break;
	}
}

static inline int adjust_x_scale(double val)
{
	int res = (int) val * (SCREEN_WIDTH / MAX_RAND);
	return res >= SCREEN_WIDTH ? --res : res;
}

static inline int adjust_y_scale(double val)
{
	int res = (int) abs(SCREEN_HEIGHT - val * (SCREEN_HEIGHT / MAX_RAND));
	return res >= SCREEN_HEIGHT ? --res : res;
}

static void plot_points(SDL_Surface *screen, int runs, int moves)
{
	int i, x, y;
	struct kpoint *current;
	SDL_Rect rect;

	rect.x = rect.y = 0;
	rect.w = screen->w;
	rect.h = screen->h;
	SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 0, 0, 0));
	SDL_Flip(screen);

	/* UGLY!! Isn't there better way to draw a points?? */

	for (i = 0; i < npoints; i++) {
		__u32 color;
		current = &points[i];
		x = adjust_x_scale(current->coord[0]);
		y = adjust_y_scale(current->coord[1]);

		color = current->affinity->color;

		putpixel(screen, x, y, color);
		if (x + 1 < SCREEN_WIDTH)
			putpixel(screen, x+1, y, color);
		if (y + 1 < SCREEN_HEIGHT)
			putpixel(screen, x, y+1, color);
		if (x + 1 < SCREEN_WIDTH && y + 1 < SCREEN_HEIGHT)
			putpixel(screen, x+1, y+1, color);
	}

	for (i = 0; i < nclusters; i++) {
		current = clusters[i].center;
		x = adjust_x_scale(current->coord[0]);
		y = adjust_y_scale(current->coord[1]);

		if (x - 1 >= 0 && y - 1 >= 0)
			putpixel(screen, x-1, y-1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (y - 1 >= 0)
			putpixel(screen, x,   y-1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (x + 1 < SCREEN_WIDTH && y - 1 >= 0)
			putpixel(screen, x+1, y-1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (x - 1 >= 0)
			putpixel(screen, x-1, y,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		putpixel(screen, x,   y,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (x + 1 < SCREEN_WIDTH)
		putpixel(screen, x+1, y,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (x - 1 >= 0 && y + 1 < SCREEN_HEIGHT)
			putpixel(screen, x-1, y+1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (y + 1 < SCREEN_HEIGHT)
			putpixel(screen, x,   y+1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
		if (x + 1 < SCREEN_WIDTH && y + 1 < SCREEN_HEIGHT)
			putpixel(screen, x+1, y+1,
				SDL_MapRGB(screen->format, 0xff, 0xff, 0xff));
	}

	SDL_UpdateRect(screen, 0, 0, 0, 0);
}

static void print_usage(char *exe)
{
	fprintf(stderr, "%s <n_threads> <n_clusters> <datafile>\n", exe);
}

static char linebuf[128];

int main(int argc, char **argv)
{
	int i, d, ch, rest, run = 0, res = 0;
	FILE *fp;
	struct work_data *data;
	struct kpoint *current_point;
	pthread_t *threads;
	SDL_Surface *screen;

	if (argc != 4) {
		print_usage(argv[0]);
		return 1;
	}

	nthreads = atoi(argv[1]);
	if (nthreads < 4) {
		fprintf(stderr, "%d is too small, will spawn 4 threads..\n",
				nthreads);
		nthreads = 4;
	}
	nclusters = atoi(argv[2]);
	if (nclusters < 2) {
		fprintf(stderr, "%d is too small, "
				"will generate 2 clusters..\n", nclusters);
		nclusters = 2;
	}

	if ((fp = fopen(argv[3], "r")) == NULL) {
		perror("fopen");
		return errno;
	}

	while (fgets(linebuf, 127, fp) != NULL) {
		if (!isspace(linebuf[0]) && linebuf[0] != '#') {
			if (dimension == 0)
				sscanf(linebuf, "%d", &dimension);
			else
				npoints++;
		}
	}
	if (ferror(fp)) {
		perror("fgets");
		res = errno;
		goto outfp;
	}

	points = (struct kpoint *) malloc(
				sizeof(struct kpoint) * (npoints + nclusters)
				+ sizeof(struct kcluster) * nclusters
				+ sizeof(struct work_data) * nthreads
				+ sizeof(pthread_t) * nthreads);
	if (!points) {
		perror("malloc");
		res = errno;
		goto outfp;
	}
	clusters = (struct kcluster *) &points[npoints + nclusters];
	data = (struct work_data *) &clusters[nclusters];
	threads = (pthread_t *) &data[nthreads];

	rewind(fp);
	fgets(linebuf, 127, fp);	/* skip the first line */

	i = 0;
	while (fgets(linebuf, 127, fp) != NULL) {
		struct kpoint *current = &points[i];
		char *pos = linebuf;

		if (isspace(linebuf[0]) && linebuf[0] == '#')
			continue;

		for (d = 0; d < dimension; d++) {
			sscanf(pos, "%lf", &current->coord[d]);
			pos = strchr(pos, ' ') + 1;
		}

		i++;
	}
	if (ferror(fp)) {
		perror("fgets");
		res = errno;
		goto outmem;
	}
	fclose(fp);
	fp = NULL;

	for (i = 0; i < nclusters; i++) {
		struct kcluster *current_cluster = &clusters[i];

		memset(current_cluster, 0, sizeof(*current_cluster));

		/* randomly pick some points for centers */
		pthread_mutex_init(&current_cluster->lock, NULL);
		current_cluster->center = &points[npoints + i];
		*current_cluster->center = points[i];
	}

	pthread_barrier_init(&barrier, NULL, nthreads + 1);

	rest = npoints % nthreads;
	current_point = points;

	for (i = 0; i < nthreads; i++) {
		struct work_data *current_data = &data[i];

		memset(current_data, 0, sizeof(*current_data));
		current_data->cluster = &clusters[i];
		current_data->points = current_point;
		current_data->npoints = npoints / nthreads;

		if (rest) {
			current_data->npoints++;
			rest--;
		}

		current_point += current_data->npoints;

		res = pthread_create(&threads[i], NULL, kmeans_work,
				current_data);
	}

	SDL_Init(SDL_INIT_VIDEO);
	SDL_WM_SetCaption("K-Means Clustering", "K-Means Clustering");
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);

	for (i = 0; i < nclusters; i++) {
		int num = rand();
		__u8 rr = ((__u8*) &num)[0];
		__u8 gg = ((__u8*) &num)[1];
		__u8 bb = ((__u8*) &num)[2];
		clusters[i].color =
			SDL_MapRGB(screen->format, rr, gg, bb);
	}

	do {
		pthread_barrier_wait(&barrier);

		plot_points(screen, ++run, moves - 1);

		printf("\nrun: %d, moves: %d\n", run, moves - 1);

		if (moves > 1) {
			for (i = 0; i < nclusters; i++)
				adjust_center(&clusters[i]);
			moves = 1;
		}
		else
			moves = 0;

		dump_clusters();

		sleep(1);

		pthread_barrier_wait(&barrier);
	} while (moves);

	printf("No more moving, clustering finished in %d runs. "
		"Press ENTER to terminate..", run);
	fflush(stdout);
	ch = fgetc(stdin);

	SDL_Quit();

	for (i = 0; i < nclusters; i++)
		pthread_mutex_destroy(&clusters[i].lock);

	for (i = 0; i < nthreads; i++)
		pthread_join(threads[i], (void **) NULL);

	pthread_barrier_destroy(&barrier);

outmem:
	if (points)
		free(points);
outfp:
	if (fp)
		fclose(fp);
	return res;
}

