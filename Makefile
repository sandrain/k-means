#
# Makefile
#

CC	= gcc
CFLAGS	= -g -Wall
LDFLAGS	= -lm -lpthread
SDLFLAGS = $(shell sdl-config --cflags --libs)

TARGETS = gdata kmeans

all: $(TARGETS)

gdata: generate_data.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

kmeans: kmeans.c
	$(CC) $(CFLAGS) $(LDFLAGS) $(SDLFLAGS) -o $@ $^

clean:
	@rm -f $(TARGETS)
