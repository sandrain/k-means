#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Generating random double */
#define RS_SCALE (1.0 / (1.0 + RAND_MAX))

double drand(void)
{
	double d;

	do {
		d = (((rand() * RS_SCALE) + rand()) * RS_SCALE
				+ rand()) * RS_SCALE;
	} while (d >= 1); /* Round off */
	return d;
}

#define irand(x) ((double) (rand() % (x)) * drand())

int main(int argc, char **argv)
{
	int i;
	int dimension;
	int npoints;

	if (argc != 3) {
		printf("%s <dimension> <number of points>\n", argv[0]);
		return 1;
	}

	dimension = atoi(argv[1]);
	npoints = atoi(argv[2]);

	srand(time(NULL));

	printf("%d\n", dimension);

	for (i = 0; i < npoints; i++)
		printf("%lf %lf %lf\n",
			irand(100), irand(100), irand(100));

	return 0;
}

